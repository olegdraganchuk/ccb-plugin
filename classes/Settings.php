<?php
/**
 * Cookie Consent Banner Settings Class
 * @category Wordpress Plugins
 * @package  Cookie Consent Banner
 * @author   Oleg Draganchuk <oleg.draganchuk@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace ccb;

class Settings
{
    public $options;
    public $geo;

    /**
     * Settings page constructor
     */
    public function __construct()
    {
        $this->geo = new Geo();
        $this->options = new \stdClass();
    }

    /**
     * Setting default banner view options during plugin instalation
     *
     * @return void
     */
    public function setDefaultOptions()
    {
        add_option('ccb_icon', plugins_url('../public/img/ccb_icon_empty.png', __FILE__));
        add_option(
            'ccb_content',
            array('default' => 'Our Website uses cookies [cbb_term_link]the cookie policy[/cbb_term_link]')
        );
        add_option('ccb_bg_color', '#1e73be');
        add_option('ccb_button_color', '#eeee22');
        add_option('ccb_term_link', '/privacy-policy');
    }

    /**
     * Getting current options from worprese option table
     *
     * @return object
     */
    public function getOptions()
    {
        $this->options->icon=get_option('ccb_icon');
        $this->options->content=get_option('ccb_content');
        $this->options->background_color=get_option('ccb_bg_color');
        $this->options->button_color=get_option('ccb_button_color');
        $this->options->term_link=get_option('ccb_term_link');
        return $this->options;
    }

    /**
     * Update option
     *
     * @param $_post $_POST array after settings form submit
     * @return void
     */
    public function updateOptions($_post)
    {
        foreach ($_post['ccb_content'] as $key => $content) {
            $_post['ccb_content'][$key]=stripcslashes($content);
        }
        update_option('ccb_icon', $_post['ccb_icon']);
        update_option('ccb_content', $_post['ccb_content']);
        update_option('ccb_bg_color', $_post['ccb_bg_color']);
        update_option('ccb_button_color', $_post['ccb_button_color']);
        update_option('ccb_term_link', $_post['ccb_term_link']);
    }

    /**
     * Clear all plugin data from wordpress options table
     *
     * @param $_post $_POST array after settings form submit
     * @return void
     */
    public function deleteAllOptions()
    {
        delete_option('ccb_content');
        delete_option('ccb_term_link');
        delete_option('ccb_icon');
        delete_option('ccb_bg_color');
        delete_option('ccb_button_color');
    }

    /**
     * Generate part of settings page with message for custom countries
     *
     * @param $_post $_POST array after settings form submit
     * @return html
     */
    public function renderCustomCountryField($_post)
    {
        extract(array(
            'post' => $_post,
            'countries'=>$this->geo->countries()
        ));
        ob_start();
        include dirname(__DIR__).'/admin/custom_country_field.php';
        $output = ob_get_clean();
        echo $output;
    }

    /**
     * Render settings page
     *
     * @return html
     */
    public function render()
    {
        extract(array('settings' => $this));
        ob_start();
        include dirname(__DIR__).'/admin/settings_form.php';
        $output = ob_get_clean();
        echo $output;
    }
}

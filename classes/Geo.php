<?php
/**
 * Cookie Consent Banner Geo Class
 * @category Wordpress Plugins
 * @package  Cookie Consent Banner
 * @author   Oleg Draganchuk <oleg.draganchuk@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace ccb;

class Geo
{

    /**
     * List of countries
     *
     * @return array()
     */
    public function countries()
    {
        // !TODO: Assign list of all countries
        $countries=array(
            'ua' => 'Ukraine',
            'us' => 'USA',
            'nl' => 'Netherlands',
            'fr' => 'France',
            'se' => 'Sweden',
            'es' => 'Spain',
            'de' => 'Germany',
            'pl' => 'Poland'
        );
        return $countries;
    }

    /**
     * Detect current country code
     *
     * @return string
     */
    public function detectCountry()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // !TODO: Change to other provider because limit reached
        $requestUrl=sprintf('http://api.ipstack.com/%1s?access_key=2e64c1830657d5d5ae5f08756bcd3980', $ip);
        $res = file_get_contents($requestUrl);
        $res = json_decode($res);

        if (!empty($res->country_code)) {
            return strtolower($res->country_code);
        } else {
            return 'default';
        }
    }
}

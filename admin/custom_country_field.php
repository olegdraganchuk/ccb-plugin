<div class="ccb_content_wrapper">
    <?php if ($_post['country'] != 'default') { ?>
    <select data-for="<?=$post['textarea_id']?>" class="cbb_country_selector">
        <option value=""><?=__('Select country');?></option>
        <?php foreach ($countries as $code => $name) {
            $selected='';
            if ($post['country']==$code) {
                $selected='selected';
            }?>
            <option value="<?=$code;?>" <?=$selected?>><?=$name;?></option>
        <?php } ?>
    </select>
    <?php }

    // wyziwig field
    wp_editor(
        $post['content'],
        $post['textarea_id'],
        array(
            'media_buttons'=>0,
            'textarea_rows'=> '5',
            'textarea_name'=> 'ccb_content['.$post['country'].']'
        )
    );

    if ($_post['country'] != 'default') { ?>
    <input type="button" class="remove_custom_message button-primary" value="<?=__('Remove');?>">
    <?php } ?>
</div>
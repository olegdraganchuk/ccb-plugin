<div class='wrap'>
    <h1><?=_e('Cookie consent banner settings')?></h1>
    <form method='post'>
        <table class="form-table">
            <tbody>
                <tr>
                    <th><?=_e('Message')?></th>
                    <td>
                        <?php
                        foreach ($settings->options->content as $country => $content) {
                            $settings->renderCustomCountryField(array(
                                'textarea_id'=>'ccb_content_'.$country,
                                'content' => $content,
                                'country' => $country
                            ));
                        }
                        ?>
                        <input type="button" class="button-primary" 
                        value="<?=__('Add custom message')?>" id="add_custom_country_message"
                        data-custom-messages-count="0">
                    </td>
                </tr>
                <tr>
                    <th><?=_e('Icon')?></th>
                    <td>
                        <div class="ccb_icon_uploader">
                            <img id="ccb_icon_preview" src="<?=$settings->options->icon?>"><br>
                            <input id="upload_image_button" type="button" 
                            class="button-primary" value="<?=__('Change Image')?>" />
                            <input id="ccb_icon" type="hidden" name="ccb_icon" value="<?=$settings->options->icon?>" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th><?=_e('Link to the full version of the terms')?></th>
                    <td>
                        <input type="text" name="ccb_term_link" value="<?=$settings->options->term_link?>">
                    </td>
                </tr>
                <tr>
                    <th><?=_e('Background color')?></th>
                    <td>
                        <input
                        type="text" class="color-field" name="ccb_bg_color"
                        value="<?=$settings->options->background_color?>">
                    </td>
                </tr>
                <tr>
                    <th><?=_e('Button color')?></th>
                    <td>
                        <input type="text" class="color-field" name="ccb_button_color" 
                        value="<?=$settings->options->button_color?>">
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td><?=submit_button(__('Update options'), 'primary')?></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
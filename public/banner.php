<div class="ccb_cookie" style="background-color:<?=$banner->background_color?>">
    <div class="ccb_cookie_inner">
        <div class="cbb_icon">
                <img src="<?=$banner->icon?>">
        </div>
        <div class="ccb_notification">
            <?=$this->content?>
        </div>
        <div class="ccb_buttons">
            <a href="" class="ccb_button" id="ccb_cookie_agree" 
            style="background-color:<?=$banner->button_color?>;
            border-color:<?=$banner->button_color?>;
            color:<?=$banner->background_color?>">Accept</a>

            <a href="" class="ccb_button disagree" id="ccb_cookie_disagree"
            style="border-color:<?=$banner->button_color?>; 
            color:<?=$banner->button_color?>">Close</a>
        </div>
    </div>
</div>
<?php
/**
 * Plugin Name: Cookie consent banner
 * Description: Create popup banner about cookie consent
 * Plugin URI:  https://olegdraganchuk.com/cookie-consent-banner
 * Version:     1.0.0
 * Author:      Oleg Draganchuk
 * Author URI:  https://olegdraganchuk.com
 * Text Domain: cookie-consent-banner
 * Domain Path: /languages.
 */

namespace ccb;

require 'classes/Banner.php';
require 'classes/Settings.php';
require 'classes/Adapter.php';
require 'classes/Geo.php';

$file=__FILE__;
$baner = new Banner();
$settings = new Settings();
$adapter = new Adapter($baner, $settings, $file);
